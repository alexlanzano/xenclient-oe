DESCRIPTION = "XenClient IDL definitions + rpc stubs generation mechanism"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

PV = "0+git${SRCPV}"

SRCREV = "${AUTOREV}"
SRC_URI = "git://${OPENXT_GIT_MIRROR}/idl.git;protocol=${OPENXT_GIT_PROTOCOL};branch=${OPENXT_BRANCH} \
    file://0001-Add-vglass-specific-settings-to-the-interfa.patch \
    file://0003-vglass-idl-changes.patch \
    file://0004-annotate_qt_types.patch \
    file://0005-fix_use_of_reserved_words.patch \
    file://vglass.xml \
    file://disman.xml \
    file://poold.xml \
    file://com.citrix.xenclient.input.xml \
"
#file://0001-Add-vglass-specific-settings-to-the-interfa.patch
#file://0002-add-interfaces-for-vglass-gpu-management.patch
#file://0003-vglass-idl-changes.patch
#file://0004-annotate_qt_types.patch
#file://0005-fix_use_of_reserved_words.patch
#file://vglass.xml
#file://disman.xml
#file://poold.xml
#file://com.citrix.xenclient.input.xml

S = "${WORKDIR}/git"

inherit allarch

do_install() {
    install -m 0755 -d ${D}${idldatadir}
    install -m 0644 ${S}/interfaces/* ${D}${idldatadir}
    install -m 0644 ${WORKDIR}/disman.xml ${D}${idldatadir}
    install -m 0644 ${WORKDIR}/vglass.xml ${D}${idldatadir}
    install -m 0644 ${WORKDIR}/poold.xml ${D}${idldatadir}
    install -m 0644 ${WORKDIR}/com.citrix.xenclient.input.xml ${D}${idldatadir}/input_daemon.xml
}
