# Copyright (C) 2015 Assured Information Security, Inc.
# Recipe released under the MIT license (see COPYING.MIT for the terms)
SUMMARY = "PV Display Helper for Linux Guests"
DESCRIPTION = " \
    This loadable kernel module provides backend support for Linux \
    PV display drivers intended to interface with the Display Handler. \
    \
    This module abstracts the IVC and Display Handler communications \
    interfaces, simplifying PV display devices and reducing duplication. \
"
LICENSE  = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fc178bcd425090939a8b634d1d6a9594"
#The name of the module to be produced.
KERNEL_MODULE = "pv_display_helper"
PR = "28"
#Ensure that we have IVC both at build and runtime.
DEPENDS  = " \
    virtual/kernel \
    kernel-module-ivc \
"
RDEPENDS_${PN} = "kernel-module-ivc"
#... and ensure that Kbuild is aware of our dependencies.
MODULE_DEPENDS = "\
    ivc \
"
KERNEL_MODULE_PACKAGE_SUFFIX = ""
#Categorize the module...
SECTION  = "kernel/modules"
PRIORITY = "optional"
#Always get the latest version of the PV plugin.
SRCREV = "${AUTOREV}"
PV = "0+git${SRCPV}"
#And fetch it from our Stash server.
#Hopefully this can be moved to GitHub soon -- or we can get external Stash
#access -- so these can be built elsewhere.
SRC_URI = "git://gitlab.com/vglass/pv-display-helper.git;protocol=ssh;branch=${OPENXT_BRANCH} \
    file://compiler.patch \
"
#Build directly from the directory checked out from git.
S = "${WORKDIR}/git"
inherit oxt-module
#
# We'll use make directly to create the module-- it will, in turn, call
# Kbuild.
#
do_compile () {
    oe_runmake
}
#
# For now, we'll install the kernel module manually, rather than using the kernel
# modules_install. Later, it may be desireable to get this down to something that
# can be done just by calling "modules_install".
#
do_install () {
    oe_runmake INSTALL_MOD_PATH="${D}" modules_install
    #... and copy our header file to the kernel include directory.
    install -d "${D}${KERNEL_MODULE_INCLUDE_DIR}"
    install -m 0644 ${S}/pv_display_helper.h "${D}${KERNEL_MODULE_INCLUDE_DIR}"
    install -m 0644 ${S}/common.h "${D}${KERNEL_MODULE_INCLUDE_DIR}"
    install -m 0644 ${S}/pv_driver_interface.h "${D}${KERNEL_MODULE_INCLUDE_DIR}"
}
