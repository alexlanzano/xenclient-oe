#
# Simple functions to help with modification of kernel parameters.
#
enable_kernel_option() {
  kernel_conf_variable "${1}" "y"
}
enable_kernel_module() {
  kernel_conf_variable "${1}" "m"
}
